from django.db import models
from django.core.urlresolvers import reverse 
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils.translation import ugettext_lazy as _

def upload_location(instance,filename):
	return "movies/{}/{}".format(instance.name,filename)


class Movies(models.Model):
	name		= models.CharField(max_length = 150)
	storyline	= models.TextField()
	year		= models.DateField()
	ratings		= models.FloatField(default = 4.0, validators = [MaxValueValidator(10.0),MinValueValidator(1.0)], help_text="IMDb Rating out of 10")
	duration	= models.PositiveSmallIntegerField(help_text = "Enter duration in minutes.")
	genre		= models.TextField(help_text = "Seperate genre with commas")
	cast		= models.TextField(help_text = "Seperate cast with commas")
	poster		= models.FileField(upload_to = upload_location, default = 'movies/none.jpg' ) #intentionally used FileField
	timestamp	= models.DateTimeField(auto_now_add = True)
	updated		= models.DateTimeField(auto_now = True)
	thumbnail	= models.FileField(upload_to = upload_location, default = 'movies/none.jpg',null = True ,blank = True )

	class Meta:
		verbose_name_plural = _("Movies")


	def get_absolute_url(self):
		return reverse ("movie-detail", kwargs = { 'pk':self.pk })


	def get_genre(self):
		return self.genre.split(',')

	def get_cast(self):
		return self.cast.split(',')

	def __str__(self):
		return self.name
