from django.shortcuts import render
from django.db.models import Q
 
from .models import Movies



def movieList(request):
	template_name = "movie_list.html"
	movies = Movies.objects.all()
	print(request.GET)
	query = request.GET.get("q")
	sort_by = request.GET.get("sort_by")
	if sort_by == "year" or sort_by == "ratings":
		sort_by = '-' + sort_by
	if query:
		query = query
		movies = Movies.objects.filter(Q(name__icontains = query)|
			 Q(year__icontains = query) |
			  Q(genre__icontains = query)|
			  Q(cast__icontains = query))
		if sort_by:
			movies = movies.order_by(sort_by)
	if sort_by and not query:
		movies = movies.order_by(sort_by)
	context = {
		"list": movies
	}
	return render(request, template_name, context)


def movieDetail(request,pk):
	template_name = "movie_detail.html"
	movieItem	  = Movies.objects.get(pk= pk)
	context = {
		"movie" : movieItem,
		"list" : Movies.objects.all().order_by('?') #provides a randomized queryset
	}

	return render(request, template_name, context)